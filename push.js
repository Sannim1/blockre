//Get port number for server to listen
var port = process.env.PORT || 5000;

//Require necessary modules
var http = require('http');
var Firebase = require('firebase');
var url = require('url');

//Store references to firebase data objects
var blacklistRef = new Firebase('https://blockre.firebaseio.com/blacklist');
var pendingRef = new Firebase('https://blockre.firebaseio.com/pending');

//Set response headers
var headers = {
  'Content-Type': 'application/json',
};

//Start http server
http.createServer(function(req, res) {
 //Retrieve request url and request parameters
 var variables = url.parse(req.url, true).query;
 var pathname = req.url/*url.parse(req.url).pathname;*/
 
 //regular expression to match valid request url
 var pattern = /\?number=(?:[0-9]|[a-z]|[A-Z])+/;

 console.log("Incoming request...: " + req.url);

 //function that handles all server responses
 //takes a JSON string as input
 var server_respond = function(response) {
 	res.writeHead(200, headers);
 	console.log(response);
 	res.end(response);
 	console.log("Responded...");
 }

 //Checks if request method is GET and request url matches regular expression
 //This is to filter requests such as /favicon.ico made from a web browser before proceeding
 if (req.method == 'GET' && pattern.test(pathname)) {
 	
 	var inBlacklist = false;
 	var _error = false;

 	//checks if "number" is a key in the request parameters object and assgns its value to variable "number"
 	if ('number' in variables && (variables['number'] !== '')) {
 		var number = variables['number'].toString();
 	} else {
 		//otherwise, respond with error message
 		var response = JSON.stringify({"error" : true, "message" : "Invalid request format"});
 		_error = true;
 		server_respond(response);
 	}
 	
 	//asychronous call to blacklist firebase object to check if current number exists in blacklist
 	//if number exists, assign true to variable inBlacklist
 	blacklistRef.once('value', function(snapshot) {
		var blacklist = snapshot.val();
		if (!(_error) && blacklist.indexOf(number) > -1) {
			console.log('Number in blacklist, exiting...');
			var response = JSON.stringify({"error" : true, "message" : number + " already exists in blacklist"});
			inBlacklist = true;
			server_respond(response);
		}
	});

 	//asynchronous call to pending list firebase object
 	pendingRef.once('value', function(snapshot){
		var pendingList = snapshot.val();
		if (number in pendingList) {
			if (inBlacklist || _error) {
				return;
			};
			var numRef = pendingRef.child(number);
			if (pendingList[number] == 9) {
				updateBlackList(number, numRef);
			} else {
				updatePendingList(number, numRef);
			}
		} else {
			//add new entry to pending list where key = number and value = 1
			if (_error) {
				return
			};
			console.log('Adding new entry...');
			pendingList[number] = 1;
			pendingRef.set(pendingList, function(error) {
				if (!error) {
					var response = JSON.stringify({"error" : false, "message" : "Submitted " + number + " for review"});
					server_respond(response)
				} else {
					var response = JSON.stringify({"error" : true, "message" : "Failed to submit " + number + " for review"});
					server_respond(response)
				}
			});
		}
	});

	var updateBlackList = function(number, numRef) {
		//get existing blacklist
		blacklistRef.once('value', function(snapshot) {
			var blacklist = snapshot.val();
			//if number in blacklist, exit
			if (!(_error) && blacklist.indexOf(number) > -1) {
				console.log('Number in blacklist...exiting...');
				var response = JSON.stringify({"error" : true, "message" : "Number already exists in blacklist"});
				server_respond(response);
			} else {
				if (_error) {
					return
				};
				//if number not in blacklist, append number to blacklist
				blacklist.push(number);
				console.log(blacklist);
				//update blacklist
				blacklistRef.set(blacklist, function(error){
					if (!error) {
						//remove number from pendingList
						console.log('Removing number from pending list...');
						numRef.remove(function(r_error){
							if (!r_error) {
								var response = JSON.stringify({"error" : false, "message" : "Successfully added " + number + " to blacklist."});
								server_respond(response)
							};
						});
					} else {
						var response = JSON.stringify({"error" : true, "message" : "Unable to add " + number + " to blacklist"});
						server_respond(response);
					}
				});
			}
		});
	}

	var updatePendingList = function(number, numRef) {
		if (numRef !== '') {
			numRef.once('value', function(snapshot) {
				//get value of numRef
				var numVal = snapshot.val();
				console.log(numVal);
				//add 1 to that value
				numVal += 1;
				//set new value as value for numRef
				numRef.set(numVal, function(error) {
					if (!error) {
						console.log('Updating pending list...');
						var response = JSON.stringify({"error" : false, "message" : "Submitted " + number + " for review"});
						server_respond(response)
					} else {
						console.log('Failed to update pending list...');
						var response = JSON.stringify({"error" : true, "message" : "Failed to submit " + number + " for review"});
						server_respond(response)
					}
				});
			});
		};
	}

 } else { 	
 	//if request method is not GET, server responds with appropriate error message
 	var response = JSON.stringify({"error" : true, "message" : "Invalid request method"});
 	server_respond(response);
 }
}).listen(port);

console.log("Server running at port ".concat(port.toString()));