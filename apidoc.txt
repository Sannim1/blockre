URL : http://blockre.herokuapp.com

Request Method: GET

Parameter: "number"

Response Type: JSON (application/json)

Request Format: http://blockre.herokuapp.com?number=5000

Response Format
{
	"error": true/false,
	"message": "Server Response Message"
}